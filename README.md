# Product Rating Analysis (Kotlin)

## Packages

**.analyzer** -  **RatingAnalyzer** - main class, taking a stream of strings as input, and returning the Collected Rating results (**RatingStatistics**).<br>
**.mapper** -    builds **ProductRating** POJO from a string line<br>
**.model** -     defines **ProductRating** POJO<br>
**.report** -    extracts **ProductRatingReport** object<br>
**.stats** -     **RatingStatistics** accumulator in order to collect the final results<br>
**.util** -      **SortedList** collects N best/worst results depending on Comparator as parameter<br>
**.validator** - executes the validation of **ProductRating** POJO<br>

**RatingAnalyzeReader** builds stream of lines for **RatingAnalyzer**, and transforms the result into a report, then returning
    JSON object.<br>

**RatingAnalyzeApp** takes the "data.csv" file from *resources*, constructs the **RatingAnalyzeReader**, and executes
  the analyze.<br>

## Solution

The analyze is done in a single pass.<br>

First, the stream of lines is parsed, validated, and then collected with grouping collector, which produces the statistics per product.<br>
Second, these statistics per product are downstreamed and analyzed with the **RatingStatistics** collector.<br>
Then, the needed result is extracted, and a JSON-string is created from the report.<br>

**RatingStatistics** collector uses the **util.SortedList** to collect the best and worst rating results.<br>
**SortedList** maintains only N (=3, by default) results sorted, and therefore there is no need to sort the whole set of products.<br>

So, the complexity stays linear:<br>

    N = number of provided lines
    P = number of products <= number of provided lines
    O(N) + O(P) => O(N).

## Validation

The given solution uses the bean validation by Hibernate, with the regular expressions to define the acceptable formats. <br>
However, a possible alternative validation would be a direct validation with the regular expressions without ussage of bean validation libraries. <br>

## Tests

There are the unit tests and end-2-end test, executed on the 'resources/test.csv' data.

## Build

mvn clean package

## Run

NB: use target/product-rating-1.0-SNAPSHOT-**fat**.jar

1. To execute on the resource 'data.csv', included into 'jar' (no parameters):

    java -jar target/product-rating-1.0-SNAPSHOT-fat.jar

2. To execute on a file \<**filepath**\>:

    java -jar target/product-rating-1.0-SNAPSHOT-fat.jar \<filepath\>

