package com.assanov.rating.analyzer

import com.assanov.rating.mapper.IDataMapper
import com.assanov.rating.model.ProductRating
import com.assanov.rating.stats.IRating
import com.assanov.rating.stats.Rating
import com.assanov.rating.stats.RatingStatistics
import com.assanov.rating.validator.IBasicValidator

import java.util.IntSummaryStatistics
import java.util.stream.Collectors.summarizingInt

class RatingAnalyzer internal constructor(private val dataMapper: IDataMapper<ProductRating>,
                                          private val validator: IBasicValidator<ProductRating>,
                                          private val ratingStatisticsBuilder: RatingStatistics.Builder) {

    fun analyze(seqOfLines: Sequence<String>): RatingStatistics {

        return seqOfLines
                .map(dataMapper::toModel)
                .map(validator::validate)
                .groupBy { it.id!! }
                .mapValues {
                    it.value.stream().collect(summarizingInt({ it.rating!! }))
                }
                .mapNotNull {
                    toRating(it)
                }
                .asSequence()
                .fold(ratingStatisticsBuilder.build()) { stats, element -> stats.accumulate(element) }
    }

    fun toRating(e: Map.Entry<String, IntSummaryStatistics>): IRating {
        return Rating(e.key, e.value.getCount(), e.value.getAverage())
    }

}