package com.assanov.rating.analyzer

import com.assanov.rating.mapper.IDataMapper
import com.assanov.rating.mapper.ProductRatingMapper
import com.assanov.rating.model.ProductRating
import com.assanov.rating.stats.RatingStatistics
import com.assanov.rating.validator.BasicProductRatingValidator
import com.assanov.rating.validator.IBasicValidator

import java.util.Optional

open class RatingAnalyzerBuilder {

    private var nbOfBestRated = 3
    private var nbOfWorstRated = 3
    private var ratingMapper: IDataMapper<ProductRating>? = null
    private var validator: IBasicValidator<ProductRating>? = null

    fun nbOfBestRated(nbOfBestRated: Int): RatingAnalyzerBuilder {
        this.nbOfBestRated = nbOfBestRated
        return this
    }

    fun nbOfWorstRated(nbOfWorstRated: Int): RatingAnalyzerBuilder {
        this.nbOfWorstRated = nbOfWorstRated
        return this
    }

    fun dataMapper(ratingMapper: IDataMapper<ProductRating>): RatingAnalyzerBuilder {
        this.ratingMapper = ratingMapper
        return this
    }

    fun validator(validator: IBasicValidator<ProductRating>): RatingAnalyzerBuilder {
        this.validator = validator
        return this
    }

    fun build(): RatingAnalyzer {
        return RatingAnalyzer(
                Optional.ofNullable(ratingMapper)
                        .orElse(ProductRatingMapper(",")),
                Optional.ofNullable(validator)
                        .orElse(BasicProductRatingValidator.DefaultBuilder().build()),
                RatingStatistics.Builder(nbOfBestRated, nbOfWorstRated))
    }

}