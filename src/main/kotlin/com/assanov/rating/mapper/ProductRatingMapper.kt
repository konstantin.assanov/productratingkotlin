package com.assanov.rating.mapper

import com.assanov.rating.model.ProductRating

open class ProductRatingMapper(private val delimiter: String) : IDataMapper<ProductRating> {

    override fun toModel(line: String): ProductRating {
        val fields = line.split(delimiter.toRegex())
        return ProductRating(
                fields.getOrNull(0)?.trim(),
                fields.getOrNull( 1)?.trim(),
                fields.getOrNull( 2)?.trim(),
                parseInt(fields.getOrNull( 3)?.trim()))
    }

    fun parseInt(value: String?): Int? {
        try {
            return Integer.parseInt(value)
        } catch (e: NumberFormatException) {
            return null
        }

    }

}