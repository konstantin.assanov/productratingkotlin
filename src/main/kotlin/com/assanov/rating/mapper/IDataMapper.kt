package com.assanov.rating.mapper

interface IDataMapper<out T> {

    fun toModel(line: String): T

}