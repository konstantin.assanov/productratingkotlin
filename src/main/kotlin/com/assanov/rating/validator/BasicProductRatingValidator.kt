package com.assanov.rating.validator

import com.assanov.rating.model.InvalidProductRating
import com.assanov.rating.model.ProductRating

import javax.validation.Validation
import javax.validation.Validator

class BasicProductRatingValidator(private val beanValidator: Validator) : IBasicValidator<ProductRating> {

    override fun validate(productRating: ProductRating): ProductRating {
        return if (beanValidator.validate(productRating).isEmpty())
            productRating
        else
            InvalidProductRating(productRating)
    }

    class DefaultBuilder {

        fun build(): IBasicValidator<ProductRating> {
            return BasicProductRatingValidator(
                    Validation
                            .buildDefaultValidatorFactory()
                            .validator)
        }

    }

}
