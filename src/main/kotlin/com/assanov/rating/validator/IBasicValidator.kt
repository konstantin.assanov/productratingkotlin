package com.assanov.rating.validator

interface IBasicValidator<T> {

    fun validate(data: T): T

}
