package com.assanov.rating.report

data class ProductRatingsReport (

    val validLines: Long,
    val invalidLines: Long,

    val bestRatedProducts: List<String>,
    val worstRatedProducts: List<String>,

    val mostRatedProduct: String?,
    val lessRatedProduct: String?

)