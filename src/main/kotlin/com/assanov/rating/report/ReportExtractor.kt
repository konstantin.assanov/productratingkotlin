package com.assanov.rating.report

import com.assanov.rating.stats.RatingStatistics

class ReportExtractor {

    fun extract(stats: RatingStatistics): ProductRatingsReport {
        return ProductRatingsReport(
                stats.nbValid,
                stats.nbInvalid,
                stats.highest.list().map { it.id },
                stats.lowest.list().map { it.id },
                stats.mostFrequent?.id,
                stats.leastFrequent?.id)
    }

}
