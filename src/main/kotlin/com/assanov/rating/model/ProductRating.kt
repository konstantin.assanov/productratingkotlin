package com.assanov.rating.model

import javax.validation.constraints.Max
import javax.validation.constraints.Min
import javax.validation.constraints.NotNull
import javax.validation.constraints.Pattern

open class ProductRating(

    @get:NotNull
    @get:Pattern(regexp = "^[A-Za-z][A-Za-z0-9]*$")
    val buyerId: String? = null,

    @get:NotNull
    @get:Pattern(regexp = "^[A-Za-z][A-Za-z0-9]*$")
    val shopId: String? = null,

    @get:NotNull
    @get:Pattern(regexp = "^[A-Za-z][A-Za-z0-9\\-]*-(0?[1-9]|[1-9][0-9])$")
    val id: String? = null,        // product id

    @get:Min(1)
    @get:Max(5)
    @get:NotNull
    val rating: Int? = null

)