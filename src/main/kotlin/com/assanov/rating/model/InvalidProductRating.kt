package com.assanov.rating.model

class InvalidProductRating(val original: ProductRating)
    : ProductRating(null, null, INVALID_PRODUCT_ID, 0) {

    companion object {
        const val INVALID_PRODUCT_ID = "100"
    }

}
