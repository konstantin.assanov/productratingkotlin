package com.assanov.rating.util

import java.util.ArrayList
import java.util.Comparator

class SortedList<E>(private val comparator: Comparator<E>, val capacity: Int) {

    private val best: MutableList<E> = ArrayList<E>()

    init {
        if (capacity < 1)
            throw IllegalArgumentException("Capacity must be positive integer (> 1)")
    }

    fun add(e: E) = with(best) {

        this.add(e)
        sortWith(comparator)

        if (size > capacity) {
            remove(this[size - 1])
        }
    }

    operator fun iterator(): Iterator<E> = best.iterator()

    fun list(): List<E> = best.toList()

}
