package com.assanov.rating.stats;

interface IRating {

    val id: String

    val count: Long

    val rate: Double

    val isInvalid: Boolean

}