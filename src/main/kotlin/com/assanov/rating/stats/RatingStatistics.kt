package com.assanov.rating.stats

import com.assanov.rating.util.SortedList

import java.util.Comparator

class RatingStatistics constructor(nbOfHighest: Int, nbOfLowest: Int) {

    val highest: SortedList<IRating>

    val lowest: SortedList<IRating>

    var leastFrequent: IRating? = null

    var mostFrequent: IRating? = null

    var nbValid: Long = 0

    var nbInvalid: Long = 0

    init {
        val comparator = Comparator.comparingDouble<IRating>( { it.rate })
        lowest = SortedList(comparator, nbOfLowest)
        highest = SortedList(comparator.reversed(), nbOfHighest)
    }

    fun accumulate(rating: IRating): RatingStatistics {
        if (rating.isInvalid) {
            nbInvalid += rating.count
            return this
        }

        highest.add(rating)
        lowest.add(rating)

        if (leastFrequent == null) {
            leastFrequent = rating
            mostFrequent = rating
        } else {
            if (rating.count < leastFrequent!!.count)
                leastFrequent = rating
            else if (rating.count > mostFrequent!!.count)
                mostFrequent = rating
        }

        nbValid += rating.count
        return this
    }

    open class Builder(private val nbOfHighest: Int, private val nbOfLowest: Int) {
        fun build(): RatingStatistics = RatingStatistics(nbOfHighest, nbOfLowest)
    }

}