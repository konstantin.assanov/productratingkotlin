package com.assanov.rating.stats

import com.assanov.rating.model.InvalidProductRating.Companion.INVALID_PRODUCT_ID

data class Rating (
    override val id: String,
    override val count: Long,
    override val rate: Double): IRating {

    override val isInvalid: Boolean
        get() = (INVALID_PRODUCT_ID == id)

}