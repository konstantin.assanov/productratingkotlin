package com.assanov.rating

import com.assanov.rating.analyzer.RatingAnalyzer
import com.assanov.rating.report.ProductRatingsReport
import com.assanov.rating.report.ReportExtractor
import com.assanov.rating.stats.RatingStatistics
import com.fasterxml.jackson.core.JsonProcessingException
import com.fasterxml.jackson.databind.ObjectMapper

import java.io.BufferedReader
import java.io.InputStream
import java.io.InputStreamReader
import java.util.stream.Stream
import kotlin.streams.asSequence

class RatingAnalyzeReader(private val analyzer: RatingAnalyzer,
                          private val extractor: ReportExtractor,
                          private val jsonMapper: ObjectMapper) {

    @Throws(JsonProcessingException::class)
    fun calculate(inputStream: InputStream): String {
        return toJson(extract(analyzer.analyze(streamOfLines(inputStream).asSequence())))
    }

    internal fun streamOfLines(inputStream: InputStream): Stream<String> {
        return BufferedReader(InputStreamReader(inputStream)).lines()
    }

    internal fun extract(stats: RatingStatistics): ProductRatingsReport {
        return extractor.extract(stats)
    }

    @Throws(JsonProcessingException::class)
    internal fun toJson(report: ProductRatingsReport): String {
        return jsonMapper
                .writerWithDefaultPrettyPrinter()
                .writeValueAsString(report)
    }

}