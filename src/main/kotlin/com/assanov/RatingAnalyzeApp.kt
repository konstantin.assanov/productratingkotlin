package com.assanov

import com.assanov.rating.RatingAnalyzeReader
import com.assanov.rating.analyzer.RatingAnalyzerBuilder
import com.assanov.rating.report.ReportExtractor
import com.fasterxml.jackson.databind.ObjectMapper

import java.io.FileInputStream
import java.io.FileNotFoundException
import java.io.IOException
import java.io.InputStream

@Throws(FileNotFoundException::class)
fun fin(filename: String?): InputStream
        = filename?.let { FileInputStream(it) }
            ?: object{}.javaClass.getResourceAsStream("/data.csv")


fun main(args: Array<String>) {

    val filename = args.getOrNull(0)

    val analyzer = RatingAnalyzerBuilder().build()

    val ratingReader = RatingAnalyzeReader(analyzer, ReportExtractor(), ObjectMapper())

    try {

        fin(filename).use { fin ->

            ratingReader.calculate(fin).also { json ->
                println("JSON result:\n$json")
            }

        }

    } catch (fnfe: FileNotFoundException) {

        println("File has not been found: " + fnfe.message)

    } catch (ioe: IOException) {

        println("I/O exception: " + ioe.message)

    }

}