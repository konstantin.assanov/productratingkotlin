package com.assanov.rating.model;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.junit.jupiter.params.provider.NullAndEmptySource;
import org.junit.jupiter.params.provider.NullSource;
import org.junit.jupiter.params.provider.ValueSource;

import javax.validation.Validation;
import javax.validation.Validator;

import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.junit.jupiter.params.provider.Arguments.arguments;

public class ProductRatingTest {

    Validator beanValidator = Validation
            .buildDefaultValidatorFactory()
            .getValidator();

    ProductRatingBuilder builder;

    // for buyer and shop validation

    private static Stream<Arguments> alphanumericCorrectCases() {
        return Stream.of(
                arguments("a"),     // single character
                arguments("a1"),    // letter & digit
                arguments("ABC"),    // capital letters
                arguments("a1B34cc")    // mix of digits & letters
        );
    }

    private static Stream<Arguments> alphanumericFailCases() {
        return Stream.of(
                arguments("    "),
                arguments("abscd$op"),  // not only alphanumeric
                arguments("4absd")      // not letter first
        );
    }

    // for product name validation

    private static Stream<Arguments> productCorrectCases() {
        return Stream.of(
                arguments("p-01"),     // single character
                arguments("p1-01"),    // letter & digit
                arguments("p--01"),    // letter & hyphen
                arguments("p-1-01"),   // letter & digit & hyphen
                arguments("PABC-01"),  // capital letters
                arguments("a1B--3-4cc-01")   // mix of digits & letters & hyphens
        );
    }

    private static Stream<Arguments> productFailCases() {
        return Stream.of(
                arguments("    "),
                arguments("produ$t-01"), // not only alphanumeric & hyphen
                arguments("4roduct-01"), // not letter first
                arguments("producto01"), // does not contain hyphen
                arguments("product-aa"), // not digits after hyphen at the end
                arguments("product-00"), // digits after hyphen are out of range
                arguments("product-100") // digits after hyphen are out of range
        );
    }

    @BeforeEach
    void init() {
        builder = new ProductRatingBuilder();
    }

    @Test
    void shouldSucceedIfAllCorrectValues() {
        ProductRating bean = builder.build();

        assertTrue(beanValidator.validate(bean).isEmpty());
    }

    // Succeed on Correct Buyer

    @ParameterizedTest
    @MethodSource("alphanumericCorrectCases")
    void shouldSuccedIfBuyerCorrect(String val) {
        ProductRating bean = builder.setBuyer(val).build();

        assertTrue(beanValidator.validate(bean).isEmpty());
    }

    // Fail on Incorrect Buyer

    @ParameterizedTest
    @NullAndEmptySource
    @MethodSource("alphanumericFailCases")
    void shouldFailIfBuyerIncorrect(String val) {
        ProductRating bean = builder.setBuyer(val).build();

        assertFalse(beanValidator.validate(bean).isEmpty());
    }

    // Succeed on Correct Shop

    @ParameterizedTest
    @MethodSource("alphanumericCorrectCases")
    void shouldSuccedIfShopCorrect(String val) {
        ProductRating bean = builder.setShop(val).build();

        assertTrue(beanValidator.validate(bean).isEmpty());
    }

    // Fail on Incorrect Shop

    @ParameterizedTest
    @NullAndEmptySource
    @MethodSource("alphanumericFailCases")
    void shouldFailIfShopIncorrect(String val) {
        ProductRating bean = builder.setShop(val).build();

        assertFalse(beanValidator.validate(bean).isEmpty());
    }

    // Succeed on Correct Product

    @ParameterizedTest
    @MethodSource("productCorrectCases")
    void shouldSucceedIfProductCorrect(String val) {
        ProductRating bean = builder.setProduct(val).build();

        assertTrue(beanValidator.validate(bean).isEmpty());
    }

    // Fail on Incorrect Product

    @ParameterizedTest
    @NullAndEmptySource
    @MethodSource("productFailCases")
    void shouldFailIfProductIncorrect(String val) {
        ProductRating bean = builder.setProduct(val).build();

        assertFalse(beanValidator.validate(bean).isEmpty());
    }

    // Succeed on Correct Rating

    @ParameterizedTest
    @ValueSource(ints = {1, 5})
    void shouldSucceedIfRatingCorrect(Integer val) {
        ProductRating bean = builder.setRating(val).build();

        assertTrue(beanValidator.validate(bean).isEmpty());
    }

    // Fail on Incorrect Rating

    @ParameterizedTest
    @NullSource
    @ValueSource(ints = {0, 8})
    void shouldFailIfRatingIncorrect(Integer val) {
        ProductRating bean = builder.setRating(val).build();

        assertFalse(beanValidator.validate(bean).isEmpty());
    }

    class ProductRatingBuilder {

        private String buyer = "buyer1";
        private String shop = "shop1";
        private String product = "product-01";
        private Integer rating = 3;

        ProductRatingBuilder() {}

        ProductRatingBuilder setBuyer(String buyer) {
            this.buyer = buyer;
            return this;
        }

        ProductRatingBuilder setShop(String shop) {
            this.shop = shop;
            return this;
        }

        ProductRatingBuilder setProduct(String product) {
            this.product = product;
            return this;
        }

        ProductRatingBuilder setRating(Integer rating) {
            this.rating = rating;
            return this;
        }

        ProductRating build() {
            return new ProductRating(buyer, shop, product, rating);
        }

    }

}