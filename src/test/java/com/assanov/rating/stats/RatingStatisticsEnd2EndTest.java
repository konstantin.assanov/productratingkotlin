package com.assanov.rating.stats;

import com.assanov.rating.model.InvalidProductRating;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.List;
import java.util.stream.Collectors;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

// NB: Not a unit test, all is done without mocks
public class RatingStatisticsEnd2EndTest {

    List<IRating> ratings = List.of(
        new Rating("product-01", 3, 3),
        new Rating("product-02", 5, 2.6),
        new Rating("product-03", 3, 4),
        new Rating("product-04", 2, 2),
        new Rating("product-09", 2, 1.5),
        new Rating("product-11", 1, 1),
        new Rating(InvalidProductRating.INVALID_PRODUCT_ID, 7, 0));

    RatingStatistics stats;

    @BeforeEach
    void init() {
        stats = new RatingStatistics.Builder(2, 2).build();
    }

    @Test
    void shouldCollectSuccessfully() {

        ratings.forEach(stats::accumulate);

        assertEquals(16, stats.getNbValid());
        assertEquals(7, stats.getNbInvalid());

        List<String> highest = stats.getHighest().list().stream().map(IRating::getId).collect(Collectors.toList());
        assertEquals(2, highest.size());
        assertTrue(highest.containsAll(List.of("product-03", "product-01")));

        List<String> lowest = stats.getLowest().list().stream().map(IRating::getId).collect(Collectors.toList());
        assertEquals(2, lowest.size());
        assertTrue(lowest.containsAll(List.of("product-11", "product-09")));

        assertEquals("product-02", stats.getMostFrequent().getId());
        assertEquals("product-11", stats.getLeastFrequent().getId());
    }

}