package com.assanov.rating.util;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.Comparator;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class SortedListTest {

    final int capacity = 3;
    SortedList<Integer> sortedList;

    @BeforeEach
    void init() {
        sortedList = new SortedList<Integer>(Comparator.comparingInt(Integer::intValue), capacity);
    }

    @Test
    void shouldReturnRightCapacity() {
        assertEquals(capacity, sortedList.getCapacity());
    }

    @Test
    void shouldThrowExceptionOnInvalidCapacity() {
        Assertions.assertThrows(IllegalArgumentException.class, () ->
            new SortedList<Integer>(Comparator.comparingInt(Integer::intValue),0)
        );
    }

    @Test
    void shouldAddCorrectly() {
        sortedList.add(10);

        List<Integer> saved = sortedList.list();

        assertEquals(1, saved.size());
        assertEquals(10, saved.get(0));
    }

    @Test
    void shouldOrderCorrectly() {
        sortedList.add(10);
        sortedList.add(20);
        sortedList.add(8);

        List<Integer> saved = sortedList.list();

        assertEquals(3, saved.size());

        assertEquals(8, saved.get(0));
        assertEquals(10, saved.get(1));
        assertEquals(20, saved.get(2));
    }

    @Test
    void shouldSelectCorrectly() {
        sortedList.add(10);
        sortedList.add(2);
        sortedList.add(20);
        sortedList.add(8);
        sortedList.add(4);

        List<Integer> saved = sortedList.list();

        assertEquals(3, saved.size());

        assertEquals(2, saved.get(0));
        assertEquals(4, saved.get(1));
        assertEquals(8, saved.get(2));
    }

}
