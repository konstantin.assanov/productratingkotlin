package com.assanov.rating;

import com.assanov.rating.analyzer.RatingAnalyzerBuilder;
import com.assanov.rating.report.ProductRatingsReport;
import com.assanov.rating.report.ReportExtractor;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Test;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

public class RatingAnalyzeEnd2EndTest {

    @Test
    void test() {

        RatingAnalyzeReader ratingReader = new RatingAnalyzeReader(
                new RatingAnalyzerBuilder().build(),
                new ReportExtractor(),
                new ObjectMapper());

        try (FileInputStream fin = new FileInputStream(getClass().getResource("/test.csv").getPath())) {

            String json = ratingReader.calculate(fin);

            System.out.println("JSON result:\n" + json);

            ProductRatingsReport report = new ObjectMapper().readValue(json, ProductRatingsReport.class);

            assertEquals(16, report.getValidLines());
            assertEquals(7, report.getInvalidLines());

            List<String> bestRated = report.getBestRatedProducts();
            assertEquals(3, bestRated.size());
            assertTrue(bestRated.containsAll(List.of("product-03", "product-01", "product-02")));

            List<String> worstRated = report.getWorstRatedProducts();
            assertEquals(3, worstRated.size());
            assertTrue(worstRated.containsAll(List.of("product-11", "product-09", "product-04")));

            assertEquals("product-02", report.getMostRatedProduct());
            assertEquals("product-11", report.getLessRatedProduct());

        } catch (FileNotFoundException fnfe) {

            System.out.println("File has not been found: " + fnfe.getMessage());

        } catch (IOException ioe) {

            System.out.println("I/O exception: " + ioe.getMessage());

        }

    }

}