package com.assanov.rating.mapper;

import com.assanov.rating.model.ProductRating;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class ProductRatingMapperTest {

    ProductRatingMapper mapper;

    @BeforeEach
    void init() {
        mapper = new ProductRatingMapper(",");
    }

    @Test
    void shouldReturnNullIfNullInputString() {
        Integer val = mapper.parseInt(null);

        assertNull(val);
    }

    @Test
    void shouldReturnNullIfEmptyInputString() {
        Integer val = mapper.parseInt("  ");

        assertNull(val);
    }

    @Test
    void shouldReturnNullIfInvalidInputString() {
        Integer val = mapper.parseInt("antoine");

        assertNull(val);
    }

    @Test
    void shouldReturnRightIfCorrectNumberString() {
        Integer val = mapper.parseInt("451");

        assertEquals(451, val);
    }

    @Test
    void shouldReturnRightIfNegativeNumberString() {
        Integer val = mapper.parseInt("-451");

        assertEquals(-451, val);
    }

    @Test
    void shouldBuildCorrectDataSuccessfully() {
        ProductRating productRating = mapper.toModel(" field0   , field1   , field2 , 33 ");

        assertEquals("field0", productRating.getBuyerId());
        assertEquals("field1", productRating.getShopId());
        assertEquals("field2", productRating.getId());
        assertEquals(33, productRating.getRating());
    }

}