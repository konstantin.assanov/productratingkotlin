package com.assanov.rating.analyzer;

import com.assanov.rating.mapper.IDataMapper;
import com.assanov.rating.stats.IRating;
import com.assanov.rating.stats.RatingStatistics;
import com.assanov.rating.validator.IBasicValidator;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.HashMap;
import java.util.IntSummaryStatistics;
import java.util.Map;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.Mockito.mock;

public class RatingAnalyzerTest {

    RatingAnalyzer analyzer;

    @BeforeEach
    void init() {
        analyzer = new RatingAnalyzer(mock(IDataMapper.class), mock(IBasicValidator.class), mock(RatingStatistics.Builder.class));
    }

    @Test
    void shouldReturnCorrectRatingData() {

        Map<String, IntSummaryStatistics> map = new HashMap<>();

        map.put("product", new IntSummaryStatistics(10, 1, 10, 100));

        Map.Entry<String, IntSummaryStatistics> e = map.entrySet().iterator().next();

        IRating rating = analyzer.toRating(e);

        assertNotNull(rating);

        assertEquals("product", rating.getId());
        assertEquals(10L, rating.getCount());
        assertEquals(10.0, rating.getRate());

    }

}